package itis.quiz;


/*
	Это класс-представление рациональной дроби. Вам необходимо реализовать все методы приведенные ниже, для работы с ним.
	Вы можете добавлять собственные служебные внутренние методы, но не можете менять или удалять существующие.
	Для вашего удобства конструктор, геттеры и сеттеры уже есть.
	!МЕНЯТЬ СИГНАТУРЫ И ТИПЫ ВОЗВРАЩАЕМЫХ ЗНАЧЕНИЙ НЕЛЬЗЯ!
 */
public class RationalFraction {

	private int numerator;

	private int denominator;

	public RationalFraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}

	//сокращает дробь, насколько это возможно
	void reduce(){

	}
	//сравнивает с другой дробью (не забудьте про сокращение!)
	public boolean equals(RationalFraction otherFraction) {
		return false;
	}
	//Текстовое представление дроби
	@Override
	public String toString() {
		return "";
	}
	//перевод в десятичную дробь
	public Double toDecimalFraction(){
		return 0.0;
	}
	//выделение целой части
	public Integer getNumberPart(){
		return 0;
	}
	//сложение с другой дробью (не забудьте про сокращение здесь и во всех остальных математических операциях)
	public RationalFraction add(RationalFraction otherFraction){

		return null;
	}
	//вычитание другой дроби
	public RationalFraction sub(RationalFraction otherFraction){

		return null;
	}
	//умножение на другую дробь
	public RationalFraction multiply(RationalFraction otherFraction){

		return null;
	}
	//деление на другую дробь
	public RationalFraction divide(RationalFraction otherFraction){

		return null;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
}
