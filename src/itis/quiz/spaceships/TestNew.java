package itis.quiz.spaceships;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestNew {
    static double score = 0;



    @Test
    @DisplayName("Возвращает первый по списку самый хорошо вооруженный корабль")
    void getMostPowerfulShipFirst(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 100, 0, 3));
        testShip.add(new Spaceship("Pam", 100, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);

        boolean flag = false;
        if ((result.getFirePower()==100) && (result.getName().equals("Pum"))){
            score = score + 0.34;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Если подходящего корабля нет, возвращает null")
    void getMostPowerfulShipNull(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 0, 0, 3));
        testShip.add(new Spaceship("Pam", 0, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);
        boolean flag = false;
        if (result == null){
            score = score + 0.33;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает самый хорошо вооруженный корабль")
    void getMostPowerfulShipOne(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 78, 0, 3));
        testShip.add(new Spaceship("Pam", 100, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);

        boolean flag = false;
        if ((result.getFirePower()==100) && (result.getName().equals("Pam"))){
            score = score + 0.33;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает корабль с заданным именем")
    void getShipByNameTrue(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));

        Spaceship result = commandCenter.getShipByName(testShip, "Pam");

        boolean flag = false;
        if (result.getName().equals("Pam")){
            score = score + 0.5;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Если подходящего корабля нет, возвращает null.")
    void getShipByNameFalse(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));

        Spaceship result = commandCenter.getShipByName(testShip, "Lom");

        boolean flag = false;
        if (result == null){
            score = score + 0.5;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает только корабли с достаточно большим грузовым трюмом для перевозки груза заданного размера.")
    void getAllShipsWithEnoughCargoSpaceTrue(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 10,9));
        testShip.add(new Spaceship("Pam", 7, 21, 0));
        testShip.add(new Spaceship("Boom", 8, 30, 1));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShip, 21);
        int a = 0;
        boolean flag = false;
        if (result.size()==2){
            for (int i=0; i< result.size(); i++){
                if ((result.get(i).getCargoSpace()>=21) && (result.get(i).getName().equals("Pam") || (result.get(i).getName().equals("Boom")))){
                    a++;
                }
            }
        }
        if (a==2){
            score = score + 0.5;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Если подходящих кораблей нет, возвращает пустой список.")
    void getAllShipsWithEnoughCargoSpaceFalse(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 10,9));
        testShip.add(new Spaceship("Pam", 7, 21, 0));
        testShip.add(new Spaceship("Boom", 8, 30, 1));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShip, 50);

        boolean flag = false;
        if (result.size()==0){
            score = score + 0.5;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает только \"мирные\" корабли")
    void getSpaceshipPeace(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 40, 0, 3));
        testShip.add(new Spaceship("Pam", 0, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));


        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShip);
        int a =0;
        boolean flag = false;
        if (result.size()==2) {
            for (int i = 0; i < result.size(); i++) {
                if (result.get(i).getFirePower() == 0 && (result.get(i).getName().equals("Pam")) || (result.get(i).getName().equals("Boom"))) {
                    a++;
                }
            }
        }
        if ((a==2)) {
            score = score + 0.5;
           flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("Если подходящих кораблей нет, возвращает пустой список")
    void getSpaceshipPeaceNull(){
        CommandCenter commandCenter = new CommandCenter();
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));


        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShip);

        boolean flag = false;
        if (result.size() == 0) {
            score = score + 0.5;
            flag = true;
        }
        Assertions.assertTrue(flag);
    }

    @AfterAll
    static void afterAll(){
        System.out.println("Балл за работу: " + score);
    }


}
