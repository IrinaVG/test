package itis.quiz.spaceships;

import java.util.ArrayList;

public class Test {
    CommandCenter commandCenter;

    public Test(CommandCenter commandCenter){
    this.commandCenter = commandCenter;
    }

    static double score = 0;

    public static void main(String[] args) {
        Test test = new Test(new CommandCenter());

        System.out.println("Проверка getMostPowerfulShip");
        boolean test10 = test.getMostPowerfulShipOne();
        System.out.println("Тест 1 выполнен = " + test10);
        boolean test11 = test.getMostPowerfulShipFirst();
        System.out.println("Тест 2 выполнен = " + test11);
        boolean test12 = test.getMostPowerfulShipNull();
        System.out.println("Тест 3 выполнен = " + test12);
        System.out.println("Проверка getShipByName");
        boolean test20 = test.getShipByNameTrue();
        System.out.println("Тест 1 выполнен = " + test20);
        boolean test21 = test.getShipByNameFalse();
        System.out.println("Тест 2 выполнен = " + test21);
        System.out.println("Проверка getAllShipsWithEnoughCargoSpace");
        boolean test30 = test.getAllShipsWithEnoughCargoSpaceTrue();
        System.out.println("Тест 1 выполнен = " + test30);
        boolean test31 = test.getAllShipsWithEnoughCargoSpaceFalse();
        System.out.println("Тест 2 выполнен = " + test31);
        System.out.println("Проверка getAllCivilianShips");
        boolean test40 = test.getSpaceshipPeace();
        System.out.println("Тест 1 выполнен = " + test40);
        boolean test41 = test.getSpaceshipPeaceNull();
        System.out.println("Тест 2 выполнен = " + test41);

        if (test10 & test11 & test12){
            score = score + 0.01;
        }
        System.out.println("Балл за работу: " + score);


    }

    private boolean getMostPowerfulShipFirst(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 100, 0, 3));
        testShip.add(new Spaceship("Pam", 100, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);

        if ((result.getFirePower()==100) && (result.getName().equals("Pum"))){
            score = score + 0.33;
            return true;
        }
        return false;
    }

    private boolean getMostPowerfulShipNull(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 0, 0, 3));
        testShip.add(new Spaceship("Pam", 0, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);

        if (result == null){
            score = score + 0.33;
            return true;
        }
        return  false;
    }

    private boolean getMostPowerfulShipOne(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 78, 0, 3));
        testShip.add(new Spaceship("Pam", 100, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShip);

        if ((result.getFirePower()==100) && (result.getName().equals("Pam"))){
            score = score + 0.33;
            return true;
        }
        return false;
    }

    private  boolean getShipByNameTrue(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));

        Spaceship result = commandCenter.getShipByName(testShip, "Pam");
        if (result.getName().equals("Pam")){
            score = score + 0.5;
            return true;
        }
        return false;
    }

    private boolean getShipByNameFalse(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));

        Spaceship result = commandCenter.getShipByName(testShip, "Lom");
        if (result == null){
            score = score + 0.5;
            return true;
        }
        return false;
    }


    private boolean getAllShipsWithEnoughCargoSpaceTrue(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 10,9));
        testShip.add(new Spaceship("Pam", 7, 21, 0));
        testShip.add(new Spaceship("Boom", 8, 30, 1));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShip, 21);
        int a = 0;
        if (result.size()==2){
            for (int i=0; i< result.size(); i++){
                if ((result.get(i).getCargoSpace()>=21) && (result.get(i).getName().equals("Pam") || (result.get(i).getName().equals("Boom")))){
                    a++;
                }
            }
        }
        if (a==2){
            score = score + 0.5;
            return true;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpaceFalse(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 10,9));
        testShip.add(new Spaceship("Pam", 7, 21, 0));
        testShip.add(new Spaceship("Boom", 8, 30, 1));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShip, 50);

        if (result.size()==0){
            score = score + 0.5;
            return true;
        }
        return false;
    }

    private boolean getSpaceshipPeace(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 40, 0, 3));
        testShip.add(new Spaceship("Pam", 0, 80, 0));
        testShip.add(new Spaceship("Boom", 0, 0, 0));


        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShip);
        int a =0;
        if (result.size()==2) {
            for (int i = 0; i < result.size(); i++) {
                if (result.get(i).getFirePower() == 0 && (result.get(i).getName().equals("Pam")) || (result.get(i).getName().equals("Boom"))) {
                    a++;
                }
            }
        }
        if ((a==2)){
            score = score + 0.5;
            return true;
        }
        return false;
    }
    private boolean getSpaceshipPeaceNull(){
        ArrayList<Spaceship> testShip = new ArrayList<>();
        testShip.add(new Spaceship("Pum", 89, 0,9));
        testShip.add(new Spaceship("Pam", 7, 8, 0));
        testShip.add(new Spaceship("Boom", 8, 9, 1));


        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShip);
        if (result.size() == 0){
            score = score + 0.5;
            return true;
        }
        return false;
    }
}
