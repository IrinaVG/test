package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{


    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship defaultship = new Spaceship("Default", 0, 0, 0);
        for(int i = 0; i < ships.size(); i++){
            if(ships.get(i).getFirePower() > defaultship.getFirePower()){
                defaultship = ships.get(i);
            }
        }
        if(defaultship.getFirePower()==0){
            return null;
        }
        return defaultship;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for(int i = 0; i < ships.size(); i++){
            if(ships.get(i).getName().equals(name)){
                return ships.get(i);
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> cargoShips = new ArrayList<>();
        for(int i = 0; i < ships.size(); i++){
            if(ships.get(i).getCargoSpace() >= cargoSize){
                cargoShips.add(ships.get(i));
            }
        }
        return cargoShips;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        for(int i = 0; i < ships.size(); i++){
            if(ships.get(i).getFirePower() == 0){
                civilianShips.add(ships.get(i));
            }
        }
        return civilianShips;
    }

}
